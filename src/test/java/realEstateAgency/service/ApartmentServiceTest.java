package realEstateAgency.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import realEstateAgency.enums.RoomsOptions;
import realEstateAgency.model.Apartment;
import realEstateAgency.model.ApartmentAddress;
import realEstateAgency.model.Person;
import realEstateAgency.model.Room;
import realEstateAgency.service.ApartmentService;


import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class ApartmentServiceTest {
    private ApartmentService apartmentService;

    @BeforeEach
    void setUp() {
        apartmentService = new ApartmentService();
    }

    @Test
    void shouldAddApartment() {
        //given
        ApartmentAddress address = new ApartmentAddress("Sample Street", "Sample City", 123);
        Room room1 = new Room(RoomsOptions.fromInt(2),10,1);
        Room room2 = new Room(RoomsOptions.fromInt(2),11,1);
        List<Room> rooms = new ArrayList<>();
        rooms.add(room1);
        rooms.add(room2);
        Apartment apartment = new Apartment(address,rooms,1,1,1,true,true);

        //when
        boolean result = apartmentService.addApartment(apartment);

        //then
        assertThat(result).isTrue();
        assertThat(apartmentService.getApartments()).containsExactly(apartment);
    }

    @Test
    void shouldNotAddDuplicateApartment() {

        ApartmentAddress address1 = new ApartmentAddress("Sample Street", "Sample City", 123);
        ApartmentAddress address2 = new ApartmentAddress("Sample Street", "Sample City", 123);
        Room room1 = new Room(RoomsOptions.fromInt(2),10,1);
        Room room2 = new Room(RoomsOptions.fromInt(2),11,1);
        List<Room> rooms = new ArrayList<>();
        rooms.add(room1);
        rooms.add(room2);
        Apartment apartment1 = new Apartment(address1,rooms,1,1,1,true,true);
        Apartment apartment2 = new Apartment(address2,rooms,1,1,1,true,true);

        apartmentService.addApartment(apartment1);
        boolean result = apartmentService.addApartment(apartment2);

        assertThat(result).isFalse();
        assertThat(apartmentService.getApartments()).containsExactly(apartment1);
    }

    @Test
    void shouldDeleteApartmentById() {
        ApartmentAddress address1 = new ApartmentAddress("Sample Street1", "Sample City", 123);
        ApartmentAddress address2 = new ApartmentAddress("Sample Street", "Sample City", 123);
        Room room1 = new Room(RoomsOptions.fromInt(2),10,1);
        Room room2 = new Room(RoomsOptions.fromInt(2),11,1);
        List<Room> rooms = new ArrayList<>();
        rooms.add(room1);
        rooms.add(room2);
        Apartment apartment1 = new Apartment(address1,rooms,1,1,1,true,true);
        Apartment apartment2 = new Apartment(address2,rooms,1,1,1,true,true);

        apartmentService.addApartment(apartment1);
        apartmentService.addApartment(apartment2);

        boolean result = apartmentService.deleteApartmentById(apartment1.getAddress().getId());

        assertThat(result).isTrue();
        assertThat(apartmentService.getApartments()).containsExactly(apartment2);
    }

    @Test
    void shouldNotDeleteNonexistentApartment() {
        boolean result = apartmentService.deleteApartmentById(1);

        assertThat(result).isFalse();
    }

    @Test
    void shouldClearApartments() {
        ApartmentAddress address1 = new ApartmentAddress("Sample Street1", "Sample City", 123);
        ApartmentAddress address2 = new ApartmentAddress("Sample Street", "Sample City", 123);
        Room room1 = new Room(RoomsOptions.fromInt(2),10,1);
        Room room2 = new Room(RoomsOptions.fromInt(2),11,1);
        List<Room> rooms = new ArrayList<>();
        rooms.add(room1);
        rooms.add(room2);
        Apartment apartment1 = new Apartment(address1,rooms,1,1,1,true,true);
        Apartment apartment2 = new Apartment(address2,rooms,1,1,1,true,true);

        apartmentService.addApartment(apartment1);
        apartmentService.addApartment(apartment2);

        apartmentService.clearMapApartments();

        assertThat(apartmentService.getApartments()).isEmpty();
    }
}