package realEstateAgency.service;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;
import realEstateAgency.model.Person;

import java.util.Collection;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class PersonServiceTest {
    @ParameterizedTest
    @ArgumentsSource(PersonTestDataProvider.class)
    void shouldGetPeople(final Person person) {
        // given
        final PersonService personService = new PersonService();
        personService.addPerson(person);

        //when
        Collection<Person> people = personService.getPeople();

        //then
        assertThat(people).containsExactlyInAnyOrder(person);

    }

    @ParameterizedTest
    @ArgumentsSource(PersonTestDataProvider.class)
    void shouldClearMapPeople(final Person person) {
        // given
        final PersonService personService = new PersonService();
        personService.addPerson(person);

        //when
        Collection<Person> people = personService.getPeople();
        personService.clearMapPeople();

        //then
        assertThat(people).isEmpty();

    }


    @ParameterizedTest
    @ArgumentsSource(PersonTestDataProvider.class)
    void shouldAddPersonSuccessfully(final Person person) {
        // given
        final PersonService personService = new PersonService();

        //when
        boolean result = personService.addPerson(person);

        //then
        assertThat(result).isTrue();

    }

    @Test
    void shouldNotAddPerson() {
        // given
        final PersonService personService = new PersonService();
        final Person person1 = new Person(1, "Jan", "Kowalski", "49060519927");
        final Person person2 = new Person(2, "Jan", "Kowalski", "49060519927");
        personService.addPerson(person1);

        //when
        boolean result = personService.addPerson(person2);

        //then
        assertThat(result).isFalse();

    }


    @ParameterizedTest
    @ArgumentsSource(PersonTestDataProvider.class)
    void shouldDeletePersonSuccessfully(final Person person) {
        // given
        final PersonService personService = new PersonService();

        //when
        boolean result = personService.deletePerson(person.getPesel());

        //then
        assertThat(result).isFalse();

    }

    @Test
    void shouldNotDeletePerson() {
        // given
        final PersonService personService = new PersonService();
        final Person person = new Person(1, "Jan", "Kowalski", "49060519927");

        //when
        boolean result = personService.deletePerson(person.getPesel());

        //then
        assertThat(result).isFalse();

    }

    private static class PersonTestDataProvider implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(final ExtensionContext extensionContext) {
            return Stream.of(
                    Arguments.of(new Person(1, "Jan", "Kowalski", "49060519927")),
                    Arguments.of(new Person(2, "Karina", "Brzytwa", "04250577491")),
                    Arguments.of(new Person(3, "Bożydar", "Iwanow", "89090477574")),
                    Arguments.of(new Person(4, "Gal", "Anonim", "67071232264")),
                    Arguments.of(new Person(5, "Karolina", "Nowotko", "49081559418")));
        }
    }
}