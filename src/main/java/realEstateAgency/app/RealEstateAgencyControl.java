package realEstateAgency.app;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import realEstateAgency.enums.AppOptions;
import realEstateAgency.exception.PersonAlreadyExistsException;
import realEstateAgency.io.*;
import realEstateAgency.model.Apartment;
import realEstateAgency.model.Contract;
import realEstateAgency.model.Person;
import realEstateAgency.service.ApartmentService;
import realEstateAgency.service.ContractService;
import realEstateAgency.service.PersonService;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class RealEstateAgencyControl {
    private final PrinterConsole printer = new PrinterConsole();
    private final ReaderConsole reader = new ReaderConsole();
    private final PersonService personService = new PersonService();
    private final ApartmentService apartmentService = new ApartmentService();
    private final PersonReader personReader = new PersonReader(printer);
    private final ApartmentReader apartmentReader = new ApartmentReader(printer);
    private final ContractReader contractReader = new ContractReader(personService, apartmentService, printer);
    private final ContractService contractService = new ContractService();
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final String fileNamePerson = "Person.json";
    private final String fileNameApartment = "Apartment.json";
    private final String fileNameContract = "Contract.json";


    public void controlLoop() {
        AppOptions options;

        do {
            printOptions();
            options = getOption();
            switch (options) {
                case ADD_PERSON -> addPerson();
                case SHOW_PERSON -> printPeople();
                case ADD_APARTMENT -> addApartment();
                case ADD_CONTRACT -> addContract();
                case SHOW_APARTMENT -> printApartments();
                case SHOW_CONTRACT -> printContracts();
                case DELETE_PERSON -> deletePerson();
                case DELETE_APARTMENT -> deleteApartment();
                case EXIT -> exit();
                default -> printer.printLine("Nie ma takiej opcji, wprowadź ponownie");
            }
        } while (options != AppOptions.EXIT);
    }

    private void deleteApartment() {
        try {
            int apartmentIdToDelete = apartmentReader.readId();
            boolean result = apartmentService.deleteApartmentById(apartmentIdToDelete);
            if (result) {
                saveApartmentsToJson();
                printer.printLine("Apartament został usunięty");
            } else {
                printer.printLine("Nie ma takiego apartamentu");
            }
        } catch (InputMismatchException e) {
            printer.printLine("Nie udało się usunąć apartamentu, nieprawidłowe dane.");
        }
    }

    private void exit() {
        printer.printLine("Koniec programu. Do zobaczenia!");
        reader.close();
    }


    private void printPeople() {
        loadPeopleFromJson();
        personService.getPeople()
                .stream()
                .sorted(Comparator.comparingInt(Person::getId))
                .forEach(person -> System.out.println(person.getDisplayPerson()));
    }

    private void printApartments() {
        loadApartmentsFromJson();
        apartmentService.getApartments()
                .stream()
                .sorted(Comparator.comparingInt(Apartment::getId))
                .forEach(apartment -> System.out.println(apartment.getDisplayApartment()));
    }


    private AppOptions getOption() {
        boolean optionOk = false;
        AppOptions option = null;
        while (!optionOk) {
            try {
                option = AppOptions.fromInt(reader.getInt());
                optionOk = true;
            } catch (InputMismatchException e) {
                printer.printLine("Wprowadzona wartość nie jest liczbą, podaj ponownie opcję");
            } catch (NoSuchElementException e) {
                printer.printLine(e.getMessage());
            }
        }
        return option;
    }

    private void printOptions() {
        printer.printLine("\n Wybierz opcję:");
        for (AppOptions value : AppOptions.values()) {
            printer.printLine(value.toString());
        }
    }

    private void addApartment() {
        try {
            Apartment apartment = apartmentReader.readAndCreateApartment();
            boolean checkIf = apartmentService.addApartment(apartment);
            if (checkIf) {
                saveApartmentsToJson();
                printer.printLine("Apartament został dodany.");
            } else {
                printer.printLine("Apartament o podanym adresie już istnieje " + apartment.getAddress());
            }

        } catch (InputMismatchException e) {
            printer.printLine("Nie udało się dodać mieszkania, nieprawidłowe dane.");
        }
    }

    private void addPerson() {
        try {
            Person person = personReader.readAndCreatePerson();
            boolean result = personService.addPerson(person);
            if (result) {
                savePeopleToJson();
                printer.printLine("Osoba została dodana.");
            } else {
                printer.printLine("Osoba o takim numerze pesel już istnieje " + person.getPesel());
            }
        } catch (PersonAlreadyExistsException e) {
            printer.printLine(e.getMessage());
        }
    }

    private void deletePerson() {
        try {
            String peselToDelete = personReader.readPesel();
            boolean result = personService.deletePerson(peselToDelete);
            if (result) {
                savePeopleToJson();
                printer.printLine("Osoba o numerze PESEL " + peselToDelete + " została usunięta.");
            } else {
                printer.printLine("Osoba o numerze PESEL " + peselToDelete + " nie istnieje.");
            }
        } catch (InputMismatchException e) {
            printer.printLine("Nie udało się usunąć osoby, nieprawidłowe dane.");
        }
    }

    private void loadPeopleFromJson() {
        try {
            personService.clearMapPeople();
            Collection<Person> loadedPeople = objectMapper.readValue(new File(fileNamePerson),
                    new TypeReference<Collection<Person>>() {
                    });
            personService.updatePeople(loadedPeople);
        } catch (IOException e) {
            printer.printLine("Nie udało się wczytać danych z pliku JSON.");
        }
    }

    private void savePeopleToJson() {
        try {
            objectMapper.writeValue(new File(fileNamePerson), personService.getPeople());
        } catch (IOException e) {
            printer.printLine("Nie udało się zapisać danych do pliku JSON.");
        }
    }

    private void loadApartmentsFromJson() {
        try {
            apartmentService.clearMapApartments();
            Collection<Apartment> loadedApartments = objectMapper.readValue(new File(fileNameApartment),
                    new TypeReference<Collection<Apartment>>() {
                    });
            apartmentService.updateApartment(loadedApartments);
        } catch (IOException e) {
            printer.printLine("Nie udało się wczytać danych z pliku JSON.");
        }
    }

    private void saveApartmentsToJson() {
        try {
            objectMapper.writeValue(new File(fileNameApartment), apartmentService.getApartments());
        } catch (IOException e) {
            printer.printLine("Nie udało się zapisać danych do pliku JSON.");
        }
    }

    Contract contract;
    private void addContract() {
        printer.printLine("Dostępne osoby do wyboru:");
        printPeople();
        printer.printLine("");
        printer.printLine("Dostępne apartamenty do wyboru:");
        printApartments();
        printer.printLine("");
        try {
            contract = contractReader.readAndCreateContract();
            boolean result = contractService.addContract(contract);
            if (result) {
                saveContractsToJson();
                printer.printLine("Kontrakt został dodany");
            } else {
                printer.printLine("Nie udało się dodać kontraktu");
            }
        } catch (InputMismatchException e) {
            printer.printLine("Nie udało się dodać kontraktu, nieprawidłowe dane.");
        } catch (NoSuchElementException e) {
            printer.printLine(e.getMessage());
        }
    }

    private void printContracts() {
        loadContractFromJson();

        Map<Apartment, Contract> apartmentContractMap = contractService.getContracts().stream()
                .collect(Collectors.toMap(Contract::apartment, contract -> contract));

        contractService.getPersonApartmentsMap().forEach((person, apartments) -> {
            printer.printLine("Osoba: " + person.getDisplayPerson());
            printer.printLine("Posiada przypisane apartamenty:");
            apartments.forEach(apartment -> {
                Contract contract = apartmentContractMap.get(apartment);
                if (contract != null) {
                    String ownershipInfo = contract.isTheOwner() ? "Tak" : "Nie";
                    printer.printLine(apartment.getDisplayApartment() + ", Właściciel: " + ownershipInfo);
                }
            });
            System.out.println();
        });
    }



    private void saveContractsToJson() {
        try {
            objectMapper.writeValue(new File(fileNameContract), contractService.getContracts());
        } catch (IOException e) {
            printer.printLine("Nie udało się zapisać danych do pliku JSON.");
        }
    }

    private void loadContractFromJson() {
        try {
            contractService.clearMapContracts();
            Collection<Contract> loadedApartments = objectMapper.readValue(new File(fileNameContract),
                    new TypeReference<Collection<Contract>>() {
                    });
            contractService.updateContracts(loadedApartments);
        } catch (IOException e) {
            printer.printLine("Nie udało się wczytać danych z pliku JSON.");
        }
    }
}


