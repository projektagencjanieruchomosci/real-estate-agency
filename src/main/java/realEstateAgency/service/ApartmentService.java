package realEstateAgency.service;

import realEstateAgency.model.Apartment;
import realEstateAgency.model.ApartmentAddress;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class ApartmentService {
    private final Map<ApartmentAddress, Apartment> apartments = new HashMap<>();

    public Collection<Apartment> getApartments() {
        return apartments.values();
    }

    public void clearMapApartments(){
        apartments.clear();
    }

    public void updateApartment(Collection<Apartment> loadedApartment) {
        loadedApartment.forEach(apartment -> apartments.put(apartment.getAddress(), apartment));
    }

    public boolean addApartment(Apartment apartment) {
        if(apartments.containsKey(apartment.getAddress())){
            return false;
        } else {
            generateNextApartmentAddressId(apartment);
            generateNextApartmentId(apartment);
            apartments.put(apartment.getAddress(), apartment);
            return true;
        }
    }

    private void generateNextApartmentAddressId(Apartment apartment) {
        int newedId = apartments.values()
                .stream()
                .map(Apartment::getAddress)
                .mapToInt(ApartmentAddress::getId)
                .max()
                .orElse(0);
        ApartmentAddress address = apartment.getAddress();
        address.setId(++newedId);
    }

    public boolean deleteApartmentById(int apartmentIdToDelete) {
       return apartments.values().removeIf(apartment -> apartment.getAddress().getId() == apartmentIdToDelete);
    }

    private void generateNextApartmentId(Apartment apartment) {
        int newId = apartments.values()
                .stream()
                .mapToInt(Apartment::getId)
                .max()
                .orElse(0);
        apartment.setId(newId + 1);
    }
}
