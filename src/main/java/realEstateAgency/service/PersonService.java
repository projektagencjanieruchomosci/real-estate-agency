package realEstateAgency.service;

import realEstateAgency.model.Person;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class PersonService {
    private final Map<String, Person> people = new HashMap<>();


    public Collection<Person> getPeople() {
        return people.values();
    }

    public void clearMapPeople() {
        people.clear();
    }

    public void updatePeople(Collection<Person> loadedPeople) {
        loadedPeople.forEach(person -> people.put(person.getPesel(), person));
    }


    public boolean addPerson(Person person) {
        if (people.containsKey(person.getPesel())) {
            return false;
        } else {
            generateNextPersonId(person);
            people.put(person.getPesel(), person);
            return true;
        }
    }

    public boolean deletePerson(String peselToDelete) {
     return people.values().removeIf(person -> person.getPesel().equals(peselToDelete));
    }

    private void generateNextPersonId(Person person) {
        int newedId = people.values()
                .stream()
                .mapToInt(Person::getId)
                .max()
                .orElse(0);
        person.setId(newedId + 1);
    }
}
