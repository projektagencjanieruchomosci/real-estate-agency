package realEstateAgency.service;

import realEstateAgency.model.Apartment;
import realEstateAgency.model.Contract;
import realEstateAgency.model.Person;

import java.util.*;
import java.util.stream.Collectors;

public class ContractService {
    private final List<Contract> contracts = new ArrayList<>();
    private final Map<Person, List<Contract>> personContractsMap = new HashMap<>();

    public boolean addContract(Contract contract) {
        if (!contracts.contains(contract)) {
            contracts.add(contract);

            personContractsMap.computeIfAbsent(contract.person(), k -> new ArrayList<>()).add(contract);

            return true;
        }
        return false;
    }

    public List<Contract> getContracts() {
        return contracts;
    }

    public void clearMapContracts() {
        personContractsMap.clear();
        contracts.clear();
    }

    public void updateContracts(Collection<Contract> loadedContracts) {
        contracts.addAll(loadedContracts);
        for (Contract contract : loadedContracts) {
            personContractsMap.computeIfAbsent(contract.person(), k -> new ArrayList<>()).add(contract);
        }
    }


    public Map<Person, List<Apartment>> getPersonApartmentsMap() {
        Map<Person, List<Apartment>> result = new HashMap<>();
        for (Map.Entry<Person, List<Contract>> entry : personContractsMap.entrySet()) {
            List<Apartment> apartments = entry.getValue().stream()
                    .map(Contract::apartment)
                    .collect(Collectors.toList());
            result.put(entry.getKey(), apartments);
        }
        return result;
    }
}
