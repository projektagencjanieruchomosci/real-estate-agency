package realEstateAgency.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public record Contract(Person person, Apartment apartment, boolean isTheOwner) {
    @JsonIgnore
    public String getDisplayContract() {
        return "Osoba: " + person + ", mieszkanie: " + apartment + ", czy jest właścicielem: " + isTheOwner;
    }
}
