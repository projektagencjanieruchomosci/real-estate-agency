package realEstateAgency.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
public class Apartment {
    private int id;
    private ApartmentAddress address;
    private List<Room> rooms;
    private int numberOfLevels;
    private int floor;
    private int price;
    private boolean basement;
    private boolean elevator;

    public Apartment(ApartmentAddress address, List<Room> rooms, int numberOfLevels, int floor, int price, boolean basement, boolean elevator) {
        this.address = address;
        this.rooms = rooms;
        this.numberOfLevels = numberOfLevels;
        this.floor = floor;
        this.price = price;
        this.basement = basement;
        this.elevator = elevator;
    }

    @JsonCreator
    public Apartment(@JsonProperty("id") int id,
                     @JsonProperty("address") ApartmentAddress address,
                     @JsonProperty("rooms") List<Room> rooms,
                     @JsonProperty("numberOfLevels") int numberOfLevels,
                     @JsonProperty("floor") int floor,
                     @JsonProperty("price") int price,
                     @JsonProperty("basement") boolean basement,
                     @JsonProperty("elevator") boolean elevator) {
        this.id = id;
        this.address = address;
        this.rooms = rooms;
        this.numberOfLevels = numberOfLevels;
        this.floor = floor;
        this.price = price;
        this.basement = basement;
        this.elevator = elevator;
    }

    @JsonIgnore
    public String getDisplayApartment() {
        String basementStr = basement ? "tak" : "nie";
        String elevatorStr = elevator ? "tak" : "nie";

        return "id: " + id + ". Adres: " + address.getDisplayAddress() +
                (rooms.isEmpty() ? "" : ", pomieszczenia: " + roomDescriptions()) +
                ", liczba kondygnacji: " + numberOfLevels +
                ", piętro: " + floor +
                ", isTheOwner: " + basementStr +
                ", winda: " + elevatorStr +
                ", cena: " + price + " zł";
    }

    @JsonIgnore
    private String roomDescriptions() {
        return rooms.stream()
                .map(Room::getDisplayRoom)
                .collect(Collectors.joining(", "));
    }
}

