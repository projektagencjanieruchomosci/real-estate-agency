package realEstateAgency.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Objects;

@Data
public class ApartmentAddress {
    private int id;
    private String street;
    private String city;
    private int number;

    public ApartmentAddress(String street, String city, int number) {
        this.street = street;
        this.city = city;
        this.number = number;
    }

    @JsonCreator
    public ApartmentAddress(@JsonProperty("id") int id,
                            @JsonProperty("street") String street,
                            @JsonProperty("city") String city,
                            @JsonProperty("number") int number) {
        this.id = id;
        this.street = street;
        this.city = city;
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ApartmentAddress that = (ApartmentAddress) o;
        return number == that.number && Objects.equals(street, that.street) && Objects.equals(city, that.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(street, city, number);
    }

    @JsonIgnore
    public String getDisplayAddress() {
        return city + ", ul. " + street + " " + number;
    }
}

