package realEstateAgency.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import realEstateAgency.enums.RoomsOptions;

public record Room(RoomsOptions name, double squareMeters, int levelNumber) {
    @JsonIgnore
    public String getDisplayRoom() {
        return name.getDescription() +
                ", powierzchnia: " + squareMeters + "m2" +
                ", numer kondygnacji: " + levelNumber;
    }
}
