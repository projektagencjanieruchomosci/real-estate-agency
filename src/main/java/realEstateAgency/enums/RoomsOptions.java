package realEstateAgency.enums;

import lombok.AllArgsConstructor;

import java.util.NoSuchElementException;
import java.util.Optional;

@AllArgsConstructor
public enum RoomsOptions {
    LIVING_ROOM(0, "Pokój dzienny"),
    BATHROOM(1, "Łazienka"),
    BEDROOM(2, "Sypialnia"),
    KITCHEN(3, "Kuchnia"),
    DINING_ROOM(4, "Jadalnia"),
    OTHER_ROOM(5, "Inny pokój");

    private final int number;
    private final String description;

    public String getDescription() {
        return description;
    }

    public static RoomsOptions fromInt(int number) {
        try {
            return RoomsOptions.values()[number];
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new NoSuchElementException("Brak opcji o numerze: " + number);
        }
    }

    @Override
    public String toString() {
        return number + " - " + description;
    }
}
