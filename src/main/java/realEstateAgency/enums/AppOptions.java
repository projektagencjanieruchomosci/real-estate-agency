package realEstateAgency.enums;

import lombok.AllArgsConstructor;

import java.util.NoSuchElementException;
import java.util.Optional;

@AllArgsConstructor
public enum AppOptions {
    EXIT(0, "Wyjście z programu"),
    ADD_APARTMENT(1, "Dodaj kolejne mieszkanie"),
    ADD_PERSON(2, "Dodaj osobę"),
    ADD_CONTRACT(3, "Dodaj kontrakt"),
    SHOW_APARTMENT(4, "Wyświetl dane o mieszkaniu"),
    SHOW_PERSON(5,"Wyświetl dane o osobie"),
    SHOW_CONTRACT(6, "Wyświetl dane o kontrakcie"),
    DELETE_APARTMENT(7, "Usuń dane mieszkania"),
    DELETE_PERSON(8, "Usuń dane osoby");


    private final int number;
    private final String description;


    public static AppOptions fromInt(int number) {
        try {
            return AppOptions.values()[number];
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new NoSuchElementException("Brak opcji o numerze: " + number);
        }
    }

    @Override
    public String toString() {
        return number + " - " + description;
    }
}