package realEstateAgency.io;

import java.util.Scanner;

public class ReaderConsole {
    private Scanner sc = new Scanner(System.in);

    public boolean getBoolean() {
        String input = sc.nextLine().toLowerCase();
        if (input.equals("tak")) {
            return true;
        } else if (input.equals("nie")) {
            return false;
        } else {
            System.out.println("Proszę podać odpowiedź 'tak' lub 'nie'.");
            return getBoolean();
        }
    }

    public int getInt() {
        try {
            return sc.nextInt();
        } finally {
            sc.nextLine();
        }
    }

    public double getDouble() {
        try {
            return sc.nextDouble();
        } finally {
            sc.nextLine();
        }
    }

    public String getString() {
        return sc.nextLine();
    }

    public void close() {
        sc.close();
    }
}
