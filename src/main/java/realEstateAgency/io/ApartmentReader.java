package realEstateAgency.io;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import realEstateAgency.enums.RoomsOptions;
import realEstateAgency.model.Apartment;
import realEstateAgency.model.ApartmentAddress;
import realEstateAgency.model.Room;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.NoSuchElementException;

@RequiredArgsConstructor
public class ApartmentReader {
    private final ReaderConsole reader = new ReaderConsole();

    @NonNull
    private PrinterConsole printer;

    public Apartment readAndCreateApartment() {
        ApartmentAddress address = createAddress();
        List<Room> room = createRoom();
        printer.printLine("Podaj liczbę kondygnacji");
        int numberOfLevels = reader.getInt();
        printer.printLine("Podaj numer piętra");
        int floor = reader.getInt();
        printer.printLine("Czy ma piwnicę? (tak/nie)");
        boolean basement = reader.getBoolean();
        printer.printLine("Czy ma windę? (tak/nie)");
        boolean elevator = reader.getBoolean();
        printer.printLine("Podaj cenę");
        int price = reader.getInt();
        return new Apartment(address, room, numberOfLevels, floor, price, basement, elevator);
    }

    private List<Room> createRoom() {
        List<Room> result = new ArrayList<>();
        printer.printLine("Wprowadź liczbę pomieszczeń w mieszkaniu:");
        int numberOfRooms = reader.getInt();
        for (int i = 0; i < numberOfRooms; i++) {
            printRooms();
            RoomsOptions room = getRooms();
            printer.printLine("Podaj powierzchnię pomieszczenia w m2: ");
            double squareMeters = reader.getDouble();
            printer.printLine("Podaj numer kondygnacji: ");
            int levelNumber = reader.getInt();
            result.add(new Room(room, squareMeters, levelNumber));
        }
        return result;
    }

    private void printRooms() {
        printer.printLine("\n Wybierz pomieszczenie:");
        for (RoomsOptions value : RoomsOptions.values()) {
            printer.printLine(value.toString());
        }
    }

    private RoomsOptions getRooms() {
        boolean optionOk = false;
        RoomsOptions option = null;
        while (!optionOk) {
            try {
                option = RoomsOptions.fromInt(reader.getInt());
                optionOk = true;
            } catch (InputMismatchException e) {
                printer.printLine("Wprowadzona wartość nie jest liczbą, podaj ponownie opcję");
            } catch (NoSuchElementException e) {
                printer.printLine(e.getMessage());
            }
        }
        return option;
    }

    private ApartmentAddress createAddress() {
        printer.printLine("Podaj ulicę: ");
        String street = reader.getString();
        printer.printLine("Podaj miasto: ");
        String city = reader.getString();
        printer.printLine("Podaj numer budynku: ");
        int number = reader.getInt();
        return new ApartmentAddress(street, city, number);
    }

    public int readId() {
        printer.printLine("Podaj ID apartamentu do usunięcia: ");
        return reader.getInt();
    }
}
