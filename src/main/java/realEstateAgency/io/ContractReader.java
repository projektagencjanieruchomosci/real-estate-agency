package realEstateAgency.io;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import realEstateAgency.model.Apartment;
import realEstateAgency.model.Contract;
import realEstateAgency.model.Person;
import realEstateAgency.service.ApartmentService;
import realEstateAgency.service.PersonService;


import java.util.NoSuchElementException;

@RequiredArgsConstructor
public class ContractReader {
    private final ReaderConsole reader = new ReaderConsole();
    private final PersonService personService;
    private final ApartmentService apartmentService;

    @NonNull
    private PrinterConsole printer;

    public Contract readAndCreateContract() {
        printer.printLine("Podaj id osoby:");
        int personID = reader.getInt();
        printer.printLine("Podaj id apartamentu:");
        int apartmentId = reader.getInt();
        printer.printLine("Czy podana osoba jest właścicielem apartamentu? (tak/nie)");
        boolean owner = reader.getBoolean();

        Person individual = getPerson(personID);
        Apartment suite = getApartment(apartmentId);

        return  new Contract(individual, suite, owner);
    }

    private Apartment getApartment(int apartmentId) {
        return apartmentService.getApartments()
                .stream()
                .filter(apartment -> apartment.getId() == apartmentId)
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Nie znaleziono apartamentu o podanym ID"));
    }

    private Person getPerson(int personID) {
        return personService.getPeople()
                .stream()
                .filter(person -> person.getId() == personID)
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Nie znaleziono osoby o podanym ID"));
    }
}
