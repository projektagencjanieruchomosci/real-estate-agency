package realEstateAgency.io;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import realEstateAgency.model.Person;

@RequiredArgsConstructor
public class PersonReader {
    private final ReaderConsole reader = new ReaderConsole();


    @NonNull
    private PrinterConsole printer;

    public Person readAndCreatePerson() {
        String pesel;
        printer.printLine("Podaj imię: ");
        String firstName = reader.getString();
        printer.printLine("Podaj nazwisko: ");
        String lastName = reader.getString();
        do {
            printer.printLine("Podaj pesel: ");
            pesel = reader.getString();
            if (!isValidPesel(pesel)) {
                printer.printLine("Numer pesel jest niepoprawny, podaj jeszcze raz.");
            }
        } while (!isValidPesel(pesel));

        return new Person(firstName, lastName, pesel);
    }

    private boolean isValidPesel(String pesel) {
        if (pesel == null || pesel.length() != 11 || !pesel.matches("\\d{11}")) {
            return false;
        }
        int[] weights = {1, 3, 7, 9, 1, 3, 7, 9, 1, 3};
        int sum = 0;

        for (int i = 0; i < 10; i++) {
            sum += Character.getNumericValue(pesel.charAt(i)) * weights[i];
        }

        int controlDigit = Character.getNumericValue(pesel.charAt(10));
        int calculatedControlDigit = 10 - (sum % 10);

        if (calculatedControlDigit == 10) {
            calculatedControlDigit = 0;
        }
        return calculatedControlDigit == controlDigit;
    }

    public String readPesel() {
        printer.printLine("Podaj numer PESEL osoby do usunięcia: ");
        return reader.getString();
    }
}